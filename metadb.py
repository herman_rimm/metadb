import numpy
import math
import os
import sqlite3

# (modified from) https://danielhomola.com/learning/newtons-method-with-10-lines-of-python/
def f(x):
    return 6*x**5-5*x**4-4*x**3+3*x**2

def df(x):
    return 30*x**4-20*x**3-12*x**2+6*x

def newton(x0, i):
    while i > 0:
        i = i - 1
        x0 = x0 - f(x0)/df(x0)
    return x0

# from https://stackoverflow.com/questions/2413522/weighted-standard-deviation-in-numpy#2415343
def weighted_avg_and_std(values, weights):
        """
        Return the weighted average and standard deviation.

        values, weights -- Numpy ndarrays with the same shape.
        """
        average = numpy.average(values, weights=weights)
        # Fast and numerically precise:
        variance = numpy.average((values-average)**2, weights=weights)
        return (average, math.sqrt(variance))

# create database
os.remove('meta.db')
db = sqlite3.connect('meta.db')
cursor = db.cursor()
# TODO: take user input via gui
sql = input("SQL definitions file: ")
sql = open(sql, "r").read()
cursor.executescript(sql)

# parse workload
workload = input("workload file: ")
workload = open(workload, "r").readlines()
# Store attribute value QFs in a nested dictionary.
qfdicts = dict()
# Store QF bandwidth parameters for numerical attributes in a dictionary.
qf_hs = dict()
lines = int(workload[1].split()[0])
for line in workload[2:lines+2]:
    # Queried attribute values appear after the WHERE.
    where = line.split("WHERE")
    # frequency of query is at the start of the line
    frequency = int(where[0].split()[0])
    # TODO: Use QF of attributes themselves, i.e. how often they appear after SELECT.
    # attribute-value(s) pair(s separated by AND)
    pairs = where[1].split("AND")
    for pair in pairs:
        words = pair.strip().split()

        attribute = words[0].strip()
        if not attribute in qfdicts:
            qfdicts[attribute] = dict()
        qfdict = qfdicts[attribute]

        # Join attribute values that contained whitespace back together.
        value = ' '.join(words[2:])
        if words[1] == '=':
            values = [value.strip()]
        else: # words[1] == "IN"
            values = value.strip("()").split(',')
        for value in values:
            value = value.strip("'")
            if value in qfdict:
                qfdict[value] = qfdict[value] + frequency
            else:
                qfdict[value] = frequency

# create metadb
table_name = cursor.execute("""
    SELECT name FROM sqlite_schema 
    WHERE type = 'table' 
    AND name NOT LIKE 'sqlite_%'
    ORDER BY 1;""").fetchone()[0]
table = cursor.execute('select * from ' + table_name)
# Assume id column is present and skip over it.
attributes = [x[0] for x in table.description[1:]]
table = table.fetchall()
types = [type(x) for x in table[0][1:]]
# cursor.lastrowid() overcounts deleted table rows, len() does not.
rows = len(table)
tolerance = newton(rows, 9)
# Create table to store bandwidth parameters of numerical data.
cursor.execute('create table metadb_bandwidth (attribute text not null, bandwidth real, primary key (attribute));')
for (attribute, type) in zip(attributes, types):
    if type == float:
        sql_type = 'real'
    if type == str:
        sql_type = 'text'
    elif type == int:
        sql_type = 'integer'
    attribute_table = 'create table %s (value %s not null, idf real, primary key (value));'
    cursor.execute(attribute_table % (attribute, sql_type))
    histogram = 'select %s,count(%s) from %s group by %s'
    histogram = cursor.execute(histogram % (attribute, attribute, table_name, attribute)).fetchall()
    # If there are few unique values treat the data as categorical and numerical otherwise.
    if len(histogram) < tolerance or type == str:
        # Insert dummy bandwidth of 0 to indicate the data is categorical.
        cursor.execute('insert into metadb_bandwidth values (\'%s\', %s)' % (attribute, 0))
        for (value, count) in histogram:
            if count == 1:
                idf = tolerance
            else:
                idf = newton(rows / count, 9)
            # TODO: use executemany
            cursor.execute('insert into %s values (\'%s\', %s)' % (attribute, value, idf))
    else:
        # QF bandwidth parameter
        if attribute in qfdicts:
            qfdict = qfdicts[attribute]
            values = [float(value) for value in qfdict.keys()]
            counts = [int(count) for count in qfdict.values()]
            (_, std) = weighted_avg_and_std(values, counts)
            qf_hs[attribute] = 1.06 / rows**0.2 * std

        # bandwidth parameter
        values, weights = zip(*histogram)
        (_, std) = weighted_avg_and_std(values, weights)
        h = 1.06 / rows**0.2 * std
        cursor.execute('insert into metadb_bandwidth values (\'%s\', %s)' % (attribute, h))

        for (term, _count) in histogram:
            # exclude term itself from frequency
            frequency = -_count
            for (term_i, count) in histogram:
                contribution = math.e**-((term_i - term)**2 / (2 * h**2))
                frequency = frequency + count * contribution
            idf = newton(rows / frequency, 9)

            cursor.execute('insert into %s values (\'%s\', %s)' % (attribute, term, idf))

# Order database rows based on popularity: the sum of a row's values' QFs.
def get_popularity(row):
    popularity = 0
    for (attribute, value) in zip(attributes, row[1:]):
        if attribute in qfdicts:
            qfdict = qfdicts[attribute]
            if attribute in qf_hs:
                qf_h = qf_hs[attribute]
                qf = 0
                for (value_i, count) in qfdict.items():
                    contribution = math.e**-((float(value_i) - value)**2 / (2 * qf_h**2))
                    qf = qf + count * contribution
                popularity = popularity + newton(qf, 9)
            elif str(value) in qfdict:
                qf = qfdict[str(value)]
                popularity = popularity + newton(qf, 9)
    return popularity
table.sort(key=get_popularity)
# Create new table to insert sorted rows into.
cursor.execute('create table metadb_popular as select * from %s where false' % table_name)
format = '(?' + ''.join([',?' for x in attributes]) + ')'
cursor.executemany('insert into metadb_popular values %s' % format, table)

db.commit()
