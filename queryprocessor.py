from collections import OrderedDict
from ctypes import resize
from itertools import islice
import math
import sqlite3

# (modified from) https://danielhomola.com/learning/newtons-method-with-10-lines-of-python/
def f(x):
        return 6*x**5-5*x**4-4*x**3+3*x**2

def df(x):
        return 30*x**4-20*x**3-12*x**2+6*x

def newton(x0, i):
        while i > 0:
            i = i - 1
            x0 = x0 - f(x0)/df(x0)
        return x0

def get_similarity(bandwidth, term, idf, query):
    # numerical data
    if bandwidth > 0:
        return idf * math.e**-((term - query)**2 / (2 * bandwidth**2))
    # categorical data
    elif term == query:
        return idf
    else:
        return 0

def ita(table_name, query, k):
    # get bandwidth and idf for every query term
    idfs = dict()
    bandwidths = dict()
    similars = []
    for (attribute,value) in query:
        # The bandwidth of a numerical attribute is based on row count and standard deviation.
        bandwidth = 'select bandwidth from metadb_bandwidth where attribute = \'%s\'' % attribute
        bandwidth = cursor.execute(bandwidth).fetchone()[0]
        bandwidths[attribute] = bandwidth

        idf = 'select * from %s where value = %s' % (attribute,value)
        idf = cursor.execute(idf).fetchone()
        # numeric values specified that are not in the database,
        # will be calculated on the fly.
        if idf == None:
            histogram = 'select %s,count(%s) from metadb_popular group by %s'
            histogram = cursor.execute(histogram % (attribute, attribute, attribute)).fetchall()
            frequency = 0
            rows = 0
            for (term_i, count) in histogram:
                contribution = math.e**-((term_i - float(value))**2 / (2 * bandwidth**2))
                frequency = frequency + count * contribution
                rows = rows + count
            idf = newton(rows / frequency, 9)
        else:
            idf = idf[1]
        idfs[attribute] = idf

        # The tuples with categorical query terms and proximity to numerical terms are at the top.
        #TODO: do not load the whole table into memory
        if bandwidth > 0:
            similar = 'select * from %s order by abs(%s - %s)' % (table_name,attribute,value)
            similar = cursor.execute(similar).fetchall()
            similar.reverse()
            similars.append(similar)
        else:
            similar = 'select * from %s order by %s = %s' % (table_name,attribute,value)
            similars.append(cursor.execute(similar).fetchall())

    # Replace query attributes with attribute rowid (corresponding to column).
    columns = dict(cursor.execute('select attribute, rowid from metadb_bandwidth').fetchall())
    for (i, (attribute, value)) in enumerate(query):
        if value.isnumeric():
            value = int(value)
        else:
            value = value.strip("'")
        query[i] = (attribute, value)

    buffer = dict()
    topK = dict()
    results = 0
    while(results < k):
        threshold = 0
        for ((attribute, value), similar) in zip(query, similars):
            similar = similar.pop()
            similarity = 0
            id = similar[0]
            if (not id in buffer and not id in topK):
                for (new_attribute, value) in query:
                    bandwidth = bandwidths[new_attribute]
                    idf = idfs[new_attribute]
                    new_column = columns[new_attribute]
                    s = get_similarity(bandwidth, similar[new_column], idf, value)
                    similarity = similarity + s
                    if new_attribute == attribute:
                        threshold = threshold + s
                buffer[id] = similarity
        # move tuples in buffer that exceed the threshold to topK
        move = []
        for (id, similarity) in buffer.items():
            if similarity >= threshold:
                move.append(id)
        for id in move:
            topK[id] = buffer.pop(id)
            results = results + 1

    topK = list(topK.items())
    topK.sort(reverse=True, key=lambda x: x[1])
    return topK[:k]

# create database
db = sqlite3.connect('meta.db')
cursor = db.cursor()
raw_ceq = input("ceq:")

# process user input
if (raw_ceq.endswith(";")):
    raw_ceq = raw_ceq[:-1]
else: raise Exception("A query must end with a semicolon (;)!")
# make (attribute,value)-tuples which are seperate
def toAttVal (s):
    AttVal = s.split('=')
    return (AttVal[0].strip(' '),AttVal[1].strip(' '))
ceq = list(map(toAttVal,raw_ceq.split(',')))
k_defs = list(filter(lambda x: x[0] == "k",ceq))

if (len(k_defs) > 0): 
    k = int(k_defs[0][1])
else: 
    k = 10

ceq = list(set(ceq) - set(k_defs))
result = ita("metadb_popular", ceq, k)
entries = list()
for (uid,idf) in result:
    entry = cursor.execute("select * from metadb_popular where id = %s limit 1" % uid).fetchone()
    entries.append(entry)

for entry in entries:
    print(entry)
